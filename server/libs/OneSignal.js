import https from 'https';
const { ONESIGNAL_API_KEY } = process.env;
// DOCS https://documentation.onesignal.com/

export const sendNotification = (data) => new Promise((resolve, rejcet) => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': `Basic ${ONESIGNAL_API_KEY}`
  };

  const options = {
    host: 'onesignal.com',
    port: 443,
    path: '/api/v1/notifications',
    method: 'POST',
    headers: headers
  };

  const req = https.request(options, function(res) {  
    res.on('data', (data) => {
      resolve(JSON.parse(data));
    });
  });
  
  req.on('error', (e) => rejcet(e));
  
  req.write(JSON.stringify(data));
  req.end();
});



