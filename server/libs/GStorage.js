/* eslint-disable no-undef */
// Imports the Google Cloud client library
import Storage from '@google-cloud/storage';
import download from 'image-downloader';
import slugify from 'slugify';
import sharp from 'sharp';
import { exec } from 'child_process';

// Your Google Cloud Platform project ID
// Creates a client
const storage = new Storage({
  projectId: process.env.GSTORAGE_PROJECT_ID,
  keyFilename: process.env.GCLOUD_FILE_NAME
});

// The name for the new bucket
const bucketName = process.env.GBUCKET_NAME;

export const downloadImage = (fileUrl) => {
  let name = fileUrl.substring(fileUrl.lastIndexOf('/')+1);
  name = slugify(name.toLowerCase().replace(/[*+~.()'"!:@]/g, '-'), '-');
  return download.image({
    url:  encodeURI(fileUrl),
    dest: './public/' + name,
  })
    .then(({ filename, image }) => {
      exec('rm '+ filename);
      const compressedPath = filename.replace(/(\.png)|(\.jpg)/, '.webp');
      return sharp(image)
        .resize(800)
        .background('white')
        .embed()
        .toFormat(sharp.format.webp)
        .toFile(compressedPath)
        .then(() => compressedPath);
    });
};

export const reUpload = (fileUrl, fileName, folder) => {
  return downloadImage(fileUrl)
    .then((compressedPath) => upload(compressedPath, fileName, folder))
    .catch(err => console.log('upload', err));
};

export const upload = (filePath, fileName, folder) => {
  return storage
    .bucket(bucketName)
    .upload(filePath, {
      gzip: true,
      metadata: { cacheControl: 'public, max-age=31536000' },
      destination: `${folder}/${fileName}`,
    }).then(() => exec('rm '+ filePath));
};

