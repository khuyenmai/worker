import https from 'https';

// DOCS http://docs.accesstrade.vn/publishers/

const BASE = 'api.accesstrade.vn';
const { ACCESS_TRADE_ACCESS_KEY } = process.env;
const HEADER = {
  'Authorization': `Token ${ACCESS_TRADE_ACCESS_KEY}`,
  'Content-Type': 'application/json',
};


const setHttpParams = (path) => ({ host: BASE,  path, headers: HEADER });

const genUrlParams = (options) => {
  let url_param = '?';
  for (let attr in options) {
    url_param += `${attr}=${options[attr]}&`;
  }
  return url_param;
};

const callAPI = (method, path, options) => {
  const url_param = genUrlParams(options);
  const url = setHttpParams(path + url_param);
  return new Promise((resolve, reject) => {
    try {
      https[method](url, response => {
        let body = '';
        response.on('data', d => body += d);
        response.on('end', () => resolve(JSON.parse(body)));
      });
    } catch (e) {
      reject('Error', e);
    }
  });
};


class AccessTradeAPI  {
  transactions(options = {}) {
    return callAPI('get', '/v1/transactions', options);
  }
  campaigns(options = {}) {
    return callAPI('get', '/v1/campaigns', options);
  }
  datafeed(options = {}) {
    return callAPI('get', '/v1/datafeeds', options);
  }
  products(options = {}) {
    return callAPI('get', '/v1/datafeeds', options);
  }
  offers(options = {}) {
    //  scope	      tùy chọn	Truyền value cho tham số này là “expiring” để get các khuyến mại sắp hết hạn. Lấy tất cả nếu không truyền hoặc truyền sai value.
    //  merchant	  tùy chọn	Tên owner của khuyến mại. vd: lazada
    //  domain	    tùy chọn	Domain của khuyến mại. vd: lazada.vn
    //  coupon	    tùy chọn	Truyền value là 1 để get khuyến mại có mã giảm giá, 0 để get khuyến mại không có mã giảm giá. Mặc định lấy tất cả.
    //  status	    tùy chọn	Truyền value 1 để lấy thông tin các offers còn hoạt động, value 0 để lấy thông tin các offers hết hạn, và không truyền key này để lấy tất cả.

    options['status'] = options['status'] ? options['status'] : 1;
    return callAPI('get', '/v1/offers_informations', options);
  }

  topProducts(options = {}) {
    return callAPI('get', '/v1/top_products', options);
  }
}
export default new AccessTradeAPI();
