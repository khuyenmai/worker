/** global process */
import cron from 'node-cron';
import moment from 'moment';
import Sourcing from '../tasks/Sourcing';
import { sendNotification } from '../libs/OneSignal';
import Offer from '../models/OfferInfomation';
import { merchants } from '../../data/merchants';
import { getOffervn } from '../tasks/Offervn';

const { ONESIGNAL_APP_ID, NOTIFY_SCHEDULE = [] } = process.env;
const defaul_logo = 'https://storage.googleapis.com/sakymi/assets/res/mipmap-mdpi/ic_launcher.png';

export const getOfferTask = cron.schedule('*/1 * * * *', Sourcing.getOffers);

export const getCustomOffers = cron.schedule('0 */2 * * *', getOffervn, false);

export const sendMessages = NOTIFY_SCHEDULE.split(', ').map(schedule =>
  cron.schedule(schedule, () => {
    Offer.find({
      isSendNoti: { $exists: false },
      end_time: { $gte: moment().toDate(), $lt: moment().add(30, 'days').toDate() }
    }, null, { limit: 1, sort: { end_time: 1 }
    })
      .then((offers) => {
        if (offers.length > 0 && offers[0]) {
          const offer = offers[0];
          let merchant = merchants.find((mc) => mc.includes(offer.merchant));
          merchant = JSON.parse(merchant || '{}');
          const message = {
            app_id: ONESIGNAL_APP_ID,
            contents: { en: offer.name, vi: offer.name},
            subtitle: { en: offer.content , vi: offer.content },
            small_icon: 'ic_stat_onesignal_default',
            large_icon: merchant['website_logo'] || defaul_logo,
            big_picture: offer.image,
            android_accent_color: 'ff5722',
            data: { offerId: offer._id },
            included_segments: ['All'],
          };
          sendNotification(message)
            .then(() => {
              return Offer.update({_id: offer._id}, { $set: { isSendNoti: true }});
            })
            .then(() => console.log('Message is sent'))
            .catch(e => console.log('Message send error', e));
        }
      });
  }, false)
);