'use strict';

import CommentModel from '../models/CommentModel';

class CommentsController {
  getAll (projection, options) {
    return CommentModel.find({}, projection, options)
      .populate('author')
      .populate('belongTo.kind');
  }

  getOne (_id) {
    return CommentModel.find({ _id })
      .populate('author')
      .populate('belongTo.kind');
  }

  create (comment) {
    return CommentModel.insert(comment);
  }

  update (_id, comment) {
    return CommentModel.update({ _id }, { $set: comment });
  }

  remove (_id) {
    return CommentModel.remove({ _id });
  }

  getByProducts(prodductIds) {
    return CommentModel.find({ 'belongTo.item': { $in: prodductIds }})
      .populate('author')
      .populate('belongTo.kind');
  }
}

export default new CommentsController();
