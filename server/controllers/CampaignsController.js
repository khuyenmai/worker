import Campaign from '../models/Campaign';

class CampaignsController {
  getAll (params, projection, options) {
    return Campaign.find(params, projection, options);
  }

  getOne (_id) {
    return Campaign.find({ _id });
  }

  getBy (params) {
    return Campaign.findOne(params);
  }

  create (campaign) {
    return Campaign.insert(campaign);
  }

  update (_id, campaign) {
    return Campaign.update({ _id }, { $set: campaign });
  }

  remove (_id) {
    return Campaign.remove({ _id });
  }
}

export default new CampaignsController();
