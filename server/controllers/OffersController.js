'use strict';

import OfferInfomation from '../models/OfferInfomation';
import Sourcing from '../tasks/Sourcing';
import Campaign from '../models/Campaign';

class OffersController {
  getAll (projection = '', options = {}, merchant) {
    if (merchant) {
      return OfferInfomation.find({
        end_time: { $gte: new Date() },
        merchant: merchant,
      }, projection, options);
    }
    return Campaign.find({ approval: 'successful' }, 'merchant')
      .then((campaigns) => OfferInfomation.find({
        end_time: { $gte: new Date() },
        merchant: { $in: campaigns.map((cam) => cam.merchant)}
      }, projection, options));
  }

  getOne (_id) {
    return OfferInfomation.findOne({ _id });
  }

  create (offer) {
    return OfferInfomation.insert(offer);
  }

  update (_id, offer) {
    return OfferInfomation.update({ _id }, { $set: offer });
  }

  remove (_id) {
    return OfferInfomation.remove({ _id });
  }

  callSyncOffers () {
    return Sourcing.getOffers();
  }

  like (_id, userId) {
    return OfferInfomation.findById(_id)
      .then((offer) => {
        const { likes } = JSON.parse(JSON.stringify(offer)) || [];
        if (!likes || !likes.includes(userId)) {
          return OfferInfomation.update({ _id }, { $addToSet: { likes: userId } });
        } else {
          return OfferInfomation.update({ _id }, { $pull: { likes: userId } });
        }
      });
  }
}

export default new OffersController();
