'use strict';
import moment from 'moment';
import Product from '../models/Product';
import TopProduct from '../models/TopProduct';
import Campaign from '../models/Campaign';
import Sourcing from '../tasks/Sourcing';
import AccessTradeAPI from '../libs/AccessTradeAPI'

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

class ProductsController {
  getAll (projection, options) {
    return Campaign.find({ approval: 'successful' }, 'merchant')
      .then((campaigns) => Product.find({
        campaign: { $in: campaigns.map(({ merchant }) => merchant) }
      }, projection, options));
  }

  getAllTop(){
    return TopProduct.find({});
  }

  getOne (id) {
    return Product.findOne({ _id: id });
  }

  create (product) {
    return Product.insert(product);
  }

  update (id, product) {
    return Product.update({ _id: id }, { $set: product });
  }
  
  search(params, projection, options) {
    return Campaign.find({ approval: 'successful', scope: 'public' }, 'merchant')
      .then((campaigns) => {
        params.campaign = { $in: campaigns.map(({ merchant }) => merchant)};
        return Product.find(params, projection, options);
      });
  }

  remove (id) {
    return Product.remove({ _id: id });
  }

  syncAllProduct () {
    Campaign.find({})
      .then(async (campaigns) => {
        while (campaigns && campaigns.length > 0) {
          const campaign = campaigns.pop();
          await Sourcing.getAllProduct({
            offset: 0,
            limit: 200,
            merchant: campaign.merchant,
          });
          await Campaign.findOneAndUpdate({ _id: campaign._id }, { $set: { lastSync: new Date() }});
          await sleep(5000);
        }
        console.log('Get all data');
      })
      .catch((error) => {
        console.log('error', error);
      });
  }
  
  syncAllTopProduct () {
    return Sourcing.getTopProducts();
  }

  async syncProductsByMerchant (merchant) {
    const limit = 200;
    let offset = 0;
    let total = 0;
    do {
      console.log('merchant', merchant)
      await Campaign.findOneAndUpdate({ merchant }, { $set: { isSyncing: true }}).exec();
      const results = await AccessTradeAPI.products({ limit, offset, merchant });
      total = results.total;
      offset += limit;
      const { data = [] } = results
      while (data.length > 0) {
        const item = data.pop();
        item.price = `${item.price || '0'}`.replace(/,/g, '');
        item.update_time = moment(item.update_time, 'DD-MM-YYTHH:mm:ss') || moment();
        console.log(offset, item.name);
        await Product.findOneAndUpdate({
          url: item.url,
          product_id: item.product_id,
          sku: item.sku,
        }, {
          $set: item
        }, {
          new: true,
          upsert: true
        }).exec();
      }
      console.log('Total', `${offset}/${total}`)
      await sleep(200)
    } while(total > offset);
    await Campaign.findOneAndUpdate({ merchant }, { $set: { isSyncing: false }}).exec();
  }

  like (_id, userId) {
    return Product.update({ _id }, { $push: { likes: userId } });
  }

  unlike (_id, userId) {
    return Product.update({ _id }, { $pull: { likes: userId } });
  }
}

export default new ProductsController();