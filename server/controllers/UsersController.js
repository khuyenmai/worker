'use strict';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import User from '../models/User';
const TOKEN_KEY_SECRET = process.env.TOKEN_KEY_SECRET;

class UsersController {
  register (req, res) {
    User.findOne({
      email: req.body.email
    }, (error, user) => {
      if (error || (user && !user.service)) return res.status(400).json({ message: '' });
      var newUser = new User(req.body);
      newUser.hash_password = bcrypt.hashSync(req.body.password, 10);

      newUser.save((err, user) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          user.hash_password = undefined;
          user.following = [];
          return res.status(200).json({
            user,
            token: jwt.sign(user.toObject(), TOKEN_KEY_SECRET)
          });
        }
      });
    }
    );
  }

  signIn (req, res) {
    // console.log('User', JSON.stringify(req.body));
    if(req.body.service){
      User.findOne({ email: req.body.email, 'service.provider': req.body.service.provider })
        .then((user) => {
          return user || User.create(req.body);
        }).then((user) => {
          user.hash_password = undefined;
          user.service.user && (user.service.user.accessToken = undefined);
          user.following = [];
          return res.json({
            token: jwt.sign(user.toObject(), TOKEN_KEY_SECRET),
            user,
          });
        }).catch(() => res.status(401).json({ message: 'User Not Found' }));
    } else {
      User.findOne({
        email: req.body.email
      }, function(err, user) {
        if (err) throw err;
        if (!user) {
          return res.status(401).json({ message: 'User Not Found' });
        }
        if (user) {
          if (!user.comparePassword(req.body.password)) {
            res.status(401).json({ message: 'Wrong password' });
          } else {
            user.hash_password = undefined;
            user.following = [];
            return res.json({
              token: jwt.sign(user.toObject(), TOKEN_KEY_SECRET),
              user,
            });
          }
        }
      });
    }
  }

  parseToken (req, _res, next) {
    const { authorization = '' } = req.headers;
    let decodeUser = null;
    if (authorization && authorization !== 'Token') {
      decodeUser = jwt.verify(authorization.replace('Token ',''), TOKEN_KEY_SECRET) || {};
    }
    req.user = decodeUser;
    next();
  }

  loginRequired (req, res, next) {
    if (req.user) {
      next();
    } else {
      return res.status(401).json({ message: 'Unauthorized user!' });
    }
  }

  followCampaign (user, campaign, isFollowing) {
    // console.log(user, campaign, isFollowing)
    if (!isFollowing) {
      return User.update({ _id: user }, { $addToSet: { following: campaign }});
    }
    return User.update({ _id: user }, { $pull: { following: campaign }});
  }

  getFollowing (id) {
    return User.findOne({_id: id})
      .populate('following')
      .then((user = {}) => user.following || []);
  }
}

export default new UsersController();
