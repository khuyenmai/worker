/* eslint-disable no-console */
/* eslint-disable no-undef */
import express from 'express';
import path from 'path';
import logger from 'morgan';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import UserController from './controllers/UsersController';
import { getOfferTask, getCustomOffers } from './jobs';

const app = express();

mongoose.connect(process.env.MONGO_URL, { promiseLibrary: Promise,  useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Connetion Success'))
  .catch((err) => console.error(err));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));

console.log('ENV = ', process.env.NODE_ENV);
if (process.env.NODE_ENV !== 'development') {
  console.log('Is Build');
  app.use(express.static(path.join(__dirname, '../build')));
}
app.use(UserController.parseToken);

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

getOfferTask.start();
// sendMessages.forEach(sm => sm.start());
getCustomOffers.start();

module.exports = app;
