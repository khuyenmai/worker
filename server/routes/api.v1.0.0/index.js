import admin from './admin';

/* GET home page. */

export default (app) => {
  app.use('/api/v1.0.0/admin', admin);
};
