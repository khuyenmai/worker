import ProductsController from '../../../controllers/ProductsController';
// import Sourcing from '../../../tasks/Sourcing';

class ProductRouter {
/* GET ALL campaigns */
  getAll (req, res) {
    const options =  {
      sort: {
        discount_rate: -1
      }
    };
    ProductsController.getAll('', options)
      .then(campaigns => res.json({ success: true, data: campaigns }))
      .catch(error => res.json({ success: false, data: [] }));
  }

  //// Not REST
  callSyncProducts(req, res) {
    ProductsController.syncAllProduct();
    res.json({ success: true, data: [] });
  }
  callSyncTopProducts(req, res) {
    ProductsController.syncAllTopProduct()
      .then(() => {
        return res.json({ success: true, data: [] });
      });
  }
}

export default new ProductRouter();