import express from 'express';
var router = express.Router();
import CampaignRouter from './CampaignRouter';
import OfferRouter from './OfferRouter';
import ProductRouter from './ProductRouter';


// console.log('CampaignRouter', CampaignRouter.getAll)
router.get('/campaigns', CampaignRouter.getAll);
router.post('/sync-campaigns', CampaignRouter.callSyncCampaigns);
router.post('/sync-by-merchaint', CampaignRouter.syncByMerchaint);


router.get('/offers', OfferRouter.getAll);
router.post('/sync-offers', OfferRouter.callSyncOffers);


router.get('/products', ProductRouter.getAll);
router.post('/sync-products', ProductRouter.callSyncProducts);
router.post('/sync-top-products', ProductRouter.callSyncTopProducts);


export default router;