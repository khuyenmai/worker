// import Campaign from '../../../models/Campaign';
import CampaignsController from '../../../controllers/CampaignsController';
import Sourcing from '../../../tasks/Sourcing';
import ProductsController from '../../../controllers/ProductsController';

class CampaignRouter {
/* GET ALL campaigns */
  getAll (req, res) {
    CampaignsController.getAll()
      .then(campaigns => res.json({ success: true, data: campaigns }))
      .catch(() => res.json({ success: false, data: [] }));
  }

  // /* GET SINGLE campaigns BY ID */
  // router.get('/:id', (req, res, next) => {
  //   Campaign.findById(req.params.id, (err, post) => {
  //     if (err) return next(err);
  //     res.json(post);
  //   });
  // });

  // /* SAVE campaigns */
  // router.post('/', (req, res, next) => {
  //   Campaign.create(req.body, (err, post) => {
  //     if (err) return next(err);
  //     res.json(post);
  //   });
  // });

  // /* UPDATE campaigns */
  // router.put('/:id', (req, res, next) => {
  //   Campaign.findByIdAndUpdate(req.params.id, req.body, (err, post)  => {
  //     if (err) return next(err);
  //     res.json(post);
  //   });
  // });

  // /* DELETE campaigns */
  // router.delete('/:id', (req, res, next) => {
  //   Campaign.findByIdAndRemove(req.params.id, req.body, (err, post) => {
  //     if (err) return next(err);
  //     res.json(post);
  //   });
  // });


  //// Not REST
  callSyncCampaigns(req, res) {
    // console.log('Start Sync');
    Sourcing.getCampaigns();
    res.json({ success: true, data: [] });
  }
  syncByMerchaint (req, res) {
    const { merchant } = req.body;
    ProductsController.syncProductsByMerchant(merchant);
  }
}

export default new CampaignRouter();