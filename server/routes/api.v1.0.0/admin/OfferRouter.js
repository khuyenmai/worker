// import Campaign from '../../../models/Campaign';
import OffersController from '../../../controllers/OffersController';

class OfferRouter {
/* GET ALL campaigns */
  getAll (req, res) {
    OffersController.getAll()
      .then(offers => res.json({ success: true, data: offers }))
      .catch(error => res.json({ success: false, data: [] }));
  }

  // /* GET SINGLE campaigns BY ID */
  // router.get('/:id', (req, res, next) => {
  //   Campaign.findById(req.params.id, (err, post) => {
  //     if (err) return next(err);
  //     res.json(post);
  //   });
  // });

  // /* SAVE campaigns */
  // router.post('/', (req, res, next) => {
  //   Campaign.create(req.body, (err, post) => {
  //     if (err) return next(err);
  //     res.json(post);
  //   });
  // });

  // /* UPDATE campaigns */
  // router.put('/:id', (req, res, next) => {
  //   Campaign.findByIdAndUpdate(req.params.id, req.body, (err, post)  => {
  //     if (err) return next(err);
  //     res.json(post);
  //   });
  // });

  // /* DELETE campaigns */
  // router.delete('/:id', (req, res, next) => {
  //   Campaign.findByIdAndRemove(req.params.id, req.body, (err, post) => {
  //     if (err) return next(err);
  //     res.json(post);
  //   });
  // });


  //// Not REST
  callSyncOffers(req, res) {
    // console.log('Start Sync');
    OffersController.callSyncOffers();
    res.json({ success: true, data: [] });
  }
}

export default new OfferRouter();