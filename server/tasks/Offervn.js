var request = require('request');
const jsdom = require('jsdom');
const moment = require('moment');
import Offer from '../models/OfferInfomation';
import _ from 'lodash'

const { JSDOM } = jsdom;
const sources = {
  grabvn: {
    link: 'https://www.offers.vn/ma-khuyen-mai-grabtaxi-coupon/',
    images: [
      'https://www.grab.com/vn/wp-content/uploads/sites/11/2016/08/bike_banner.jpg',
      'https://assets.grab.com/wp-content/uploads/sites/11/2018/10/02173042/Grab-3h.jpg',
      'https://assets.grab.com/wp-content/uploads/sites/11/2018/09/28172611/GrabNow-EDM1.jpg',
      'https://www.grab.com/vn/wp-content/uploads/sites/11/2016/05/bike-2-2.jpg',
      'https://www.grab.com/vn/wp-content/uploads/sites/11/2016/05/car-2-2.jpg',
      'https://www.grab.com/vn/wp-content/uploads/sites/11/2016/05/taxi-2-2.jpg',
      'https://www.grab.com/vn/wp-content/uploads/sites/11/2016/05/02_hero_banner.jpg',
      'https://www.grab.com/vn/wp-content/uploads/sites/11/2016/05/driver.jpg',
      'https://www.grab.com/vn/wp-content/uploads/sites/11/2016/05/hero-grabrewards.jpg',
      'https://www.grab.com/vn/wp-content/uploads/sites/11/2016/05/01_hero_banner.jpg',
    ],
    aff_link: 'https://play.google.com/store/apps/details?id=com.grabtaxi.passenger',
    domain: 'www.grab.com'
  },
  "go-viet": {
    link: 'https://www.offers.vn/ma-giam-gia-goviet/',
    images: [
      'https://www.go-viet.vn/wp-content/uploads/2018/07/banner-trangchu-taixe.jpg',
      'https://www.go-viet.vn/wp-content/themes/onepress/assets/images/banner-middle-opz.jpeg',
      'https://www.go-viet.vn/wp-content/themes/onepress/assets/images/banner-signup-2.jpg',
      'https://www.go-viet.vn/wp-content/uploads/2018/06/Trangchu-screenshot-2.png',
      'https://www.go-viet.vn/wp-content/uploads/2018/07/2.jpg',
    ],
    aff_link: 'https://play.google.com/store/apps/details?id=com.goviet.app',
    domain: 'www.go-viet.vn'
  },
  nowvn: {
    link: 'https://www.offers.vn/ma-giam-gia-now-vn/',
    images: [
      'https://storage.googleapis.com/sakymi/images/bg-partnership.jpg',
      'https://storage.googleapis.com/sakymi/images/bannerhome-deliverynow-1000x375.jpg',
      'https://storage.googleapis.com/sakymi/images/Box-food-preservation-footer.jpg'
    ],
    aff_link: 'https://play.google.com/store/apps/details?id=com.deliverynow',
    domain: 'www.now.vn'
  }
};


export const getOffervn = () => {
  Promise.all(Object.keys(sources).map((name) => {
    const all_offers = [];
    const merchant = sources[name];
    return new Promise((resolve) => {
      request.get(merchant.link, (error, response, body) => {
        if (error || response.statusCode !== 200) return false;

        const dom = new JSDOM(body);
        const couponsList = dom.window.document.querySelector('.couponwrap');
        const couponsDom = couponsList.children;
        for( var i = 0; i < couponsDom.length; i++ ) {
          let sameamount = couponsDom[i].getElementsByClassName('saveamount');
          let saleorcoupon = couponsDom[i].getElementsByClassName('saleorcoupon');
          let coupontitle = couponsDom[i].getElementsByClassName('coupontitle');
          let cpinfo = couponsDom[i].getElementsByClassName('cpinfo');
          let coupon = couponsDom[i].getElementsByClassName('coupon-code');
          const offer = {
            sameamount: sameamount[0].textContent,
            saleorcoupon: saleorcoupon[0].textContent,
            coupontitle: coupontitle[0].textContent,
            cpinfo: cpinfo[0] && cpinfo[0].textContent,
            coupon: coupon[0] && coupon[0].textContent,
          };
          const end_date = (offer.cpinfo || '').match(/\d+\/\d+\/\d+/)[0];
          const infomation = {
            aid: 'custom',
            link: '',
            merchant: name,
            aff_link: merchant.aff_link,
            banners: [],
            categories: [],
            content: offer.cpinfo.trim(),
            coupons: offer.coupon ? { coupon_code: [offer.coupon] } : undefined,
            domain: merchant.domain,
            end_time: end_date && moment(end_date, 'DD/MM/YYYY').endOf('day').toDate(),
            imageName: merchant.images[i % merchant.images.length],
            name: offer.coupontitle,
          };
          all_offers.push(infomation);
        }
        resolve(all_offers);
      });
    })
  })).then(async (res) => {
    // console.log('all_offers', res)
    const all_offers = _.flattenDeep(res)
    while (all_offers.length > 0) {
      const item = all_offers.pop();
      await Offer.update(
        { aid: item.aid, merchant: item.merchant, name: item.name },
        { $set: item }, { upsert: true }
      );
    }
    console.log('OfferVn Done')
  }).catch((e) => {
    console.log('Error', e);
  })
};
