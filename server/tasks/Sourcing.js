import _ from 'lodash';
import moment from 'moment';
import uuid from 'uuid/v4';

import Product from '../models/Product';
import Campaign from '../models/Campaign';
import TopProduct from '../models/TopProduct';
import AccessTradeAPI from '../libs/AccessTradeAPI';
import OfferInfomations from '../models/OfferInfomation';
import CampaignCategory from '../models/CampaignCategory';
import { reUpload } from '../libs/GStorage'

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

class Sourcing {
  async getCampaigns () {
    const result =  await AccessTradeAPI.campaigns({});
    const campaigns = result.data;
    while (campaigns && campaigns.length > 0) {
      const campaign = campaigns.pop();
      console.log('Campaign', campaign.name);
      campaign.aid = campaign.id
      if (typeof campaign.description === 'object') {
        campaign.description = JSON.stringify(campaign.description);
      }
      await Campaign.findOneAndUpdate(
        { aid: campaign.aid, },
        { $set: campaign },
        { upsert: true, new: true, setDefaultsOnInsert: true}).exec();
      if (campaign.sub_category) {
        await CampaignCategory.update({
          name: campaign.category,
          code: campaign.category
        },
        { $addToSet:{ sub_category: campaign.sub_category } },
        { upsert: true }).exec();
      }
    }
    console.log('Done !');
  }
  getAllProduct(options) {
    let offset = options.offset || 0;
    const limit = options.limit || 200;
    const merchant = options.merchant;
    let step = 0;
    let total = 0;
    try {
      return new Promise(async (resolve, reject) => {
        const products = [];
        do {
          step += 1;
          const products = await AccessTradeAPI.products({
            limit,
            offset,
            merchant,
          });
          console.log('Products', products && products.data && products.data.length);
          console.log('Step', step);
          console.log('Total', total);
          console.log('Offset', offset);

          const { data } = products;
          total = products.total;
          /*
            cate  Danh mục sản phẩm
            desc  Mô tả chi tiết sản phẩm
            discount  giá sau khuyến mại
            campaign  chiến dịch
            domain  domain của owner sản phẩm
            image url ảnh sản phẩm
            name  tên sản phẩm
            price giá sản phẩm trước khuyến mại
            product_id  id của sản phẩm
            sku mã sản phẩm
            url link sản phẩm
            aff_link  Deep link
            status_discount trạng thái khuyến mại. 0 - ko khuyến mại, 1 - có khuyến mại
            discount_amount giá được giảm
            discount_rate phần trăm được giảm
            update_time thời gian update
          */
          if(data && data.length > 0){
            console.log('Data length', data.length);
            let proIndex = 0;
            while (proIndex < data.length ) {
              const item = data[proIndex];
              item.price = `${item.price || 0}`.replace(/,/g, '');
              item.update_time = moment(item.update_time, 'DD-MM-YYTHH:mm:ss') || moment();
              console.log('Save product', proIndex, item.name);
              await Product.findOneAndUpdate({
                url: item.url,
                product_id: item.product_id,
                sku: item.sku,
              }, {
                $set: item
              },  {
                new: true,
                upsert: true
              }).exec();
              await sleep(200);
              proIndex += 1;
            }

            console.log('Save Campaign', merchant);
            await Campaign.findOneAndUpdate({
              merchant,
            },{ $set: {
              last_offset: offset,
              total_data: total,
            }}).exec();
            offset = step * limit;
          }
          await sleep(5000);
        } while (products && (total > offset));
        console.log('No more While');
        resolve('done');
      });
    }
    catch(error) {
      return error;
    }
  }
  getProductDataByMerchain(merchant){
    AccessTradeAPI.products({merchant})
      .then(data => {
        console.log('data', data)
      })
  }
  getProductDatafeed(options ,callback){
    AccessTradeAPI.datafeed(options)
      .then((result) => {
        if(!result.data) return callback('Nodata');
        if(result.data.length == 0 || result.total == 0){
          return callback(result);
        }
        _.each(result.data, (item, index) => {
          item.price = `${item.price}`.replace(/,/g, '');
          Product.findOneAndUpdate({
            url: item.url,
            sku: item.sku,
          }, {
            $set: item
          },  {
            new: true,
            upsert: true
          }).then((err, update_result) => {
            if (err) {
              console.log('Save DataFeed Error!', err);
            }

            if(index == (result.data.length - 1)){
              Campaign.findOneAndUpdate({
                merchant: item.merchant
              },{ $set: {
                last_offset: options.offset
              }}, (erros, results)=>{
                if(erros){
                  console.log('Last Offset', erros);
                }
              });
              return callback(result);
            }
          });
        });
      });
  }
  getOffers(options) {
    console.log('GG getOffers');
    return AccessTradeAPI.offers(options)
      .then(async (result) => {
        const { data } = result;
        while (data.length > 0) {
          const item = data.pop();
          const { image } = item;
          const fileName = `${uuid()}.webp`;
          const oldItem = await OfferInfomations.findOne({ link: item.link, aid: item.id }).exec();

          item.end_time = moment(item.end_time, 'YYYY-MM-DD');
          item.start_time = moment(item.start_time, 'YYYY-MM-DD');

          console.log(item.name, item.merchant);
          if (!oldItem) {
            console.log('New Offer');
            item.createdAt = new Date();
            item.imageName = fileName;
            await reUpload(image, fileName, process.env.GSTORAGE_FOLDER);
          }

          await OfferInfomations.update(
            {
              link: item.link,
              aid: item.id,
              merchant: item.merchant
            }, {
              $set: item
            }, {
              upsert: true
            }
          );
        }
        console.log('Sync Offer done');
      })
  }
  getTopProducts(){
    return AccessTradeAPI.topProducts()
      .then(async (result) => {
        const { data: products } = result;
        if(!products || products.length === 0) { return false; }
        while(products && products.length > 0) {
          const product = products.pop();
          console.log('Product name', product.name);
          product.updatedAt = new Date();

          await TopProduct.findOneAndUpdate({
            product_id: product.product_id
          }, {
            $set: product
          }, {
            upsert: true
          });

          await sleep(200);
          if (products.length === 0) { 
            console.log('Done');
            return '';
          }
        }
      });
  }
}

export default new Sourcing();
