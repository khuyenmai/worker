import mongoose from 'mongoose';
const Schema = mongoose.Schema;


const OfferInfomationSchema = new Schema({
  banners:
  [
    {
      height: Number,
      link: String,
      width: Number
    }
  ],
  content: String,
  id: String,
  aid: String,
  image: String,
  imageName: String,
  link: String,
  merchant: String,
  start_time: Date,
  end_time: Date,
  domain: String,
  aff_link: String,
  coupons: [{
    coupon_code: String,
    coupon_desc: String,
    coupon_save: String
  }],
  categories: [{
    category_name: String,
    category_name_show: String,
    category_no: Number
  }],
  name: String,
  likes: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  isSendNoti: Boolean,
  createdAt: Date,
});

const OfferInfomations = mongoose.model('OfferInfomation', OfferInfomationSchema);
export default OfferInfomations;