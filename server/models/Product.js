import mongoose from 'mongoose';
const Schema = mongoose.Schema;
/*
    cate  Danh mục sản phẩm
    desc  Mô tả chi tiết sản phẩm
    discount  giá sau khuyến mại
    campaign  chiến dịch
    domain  domain của owner sản phẩm
    image url ảnh sản phẩm
    name  tên sản phẩm
    price giá sản phẩm trước khuyến mại
    product_id  id của sản phẩm
    sku mã sản phẩm
    url link sản phẩm
    aff_link  Deep link
    status_discount trạng thái khuyến mại. 0 - ko khuyến mại, 1 - có khuyến mại
    discount_amount giá được giảm
    discount_rate phần trăm được giảm
    update_time thời gian update
  */

const ProductSchema = new Schema({
  aid: String,
  cate: String,
  aff_link: String,
  category: String,
  category_commission: {
    partner_reward: String,
    reward_type: String
  },
  desc: String,
  discount: Number,
  campaign: String,
  domain: String,
  image: String,
  name: String,
  price: Number,
  product_id: String,
  status_discount: Number,
  discount_amount: Number,
  discount_rate: Number,
  sku: String,
  sub_category: String,
  url: String,
  update_time: Date,
  likes: [{ type: Schema.Types.ObjectId, ref: 'User' }]
});

// ProductSchema.index({name: 'text', desc: 'text', sub_category: 'text' });
const Product = mongoose.model('Product', ProductSchema);

export default Product;
