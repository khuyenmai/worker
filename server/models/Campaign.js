import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const CampaignSchema = new Schema({
  aid: String,
  icon: String,
  name: String,
  approval: String,
  merchant: String,
  category: String,
  total_data: Number,
  description: String,
  cookie_policy: String,
  url: String,
  type: Number,
  scope: String,
  end_time: Date,
  status: Number,
  start_time: Date,
  cookie_duration: Number,
  sub_category: String,
  get_data_feed_at: Date,
  conversion_policy: String,
  isSyncing: Boolean,
  last_offset: Number,
  lastSync: Date,
  likes: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  comments: [{ type: Schema.Types.ObjectId, ref: 'CommentModel' }],
});

const Campaign = mongoose.model('Campaign', CampaignSchema);

export default Campaign;
