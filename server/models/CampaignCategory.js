import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const CampaignCategorySchema = new Schema({
  aid: String,
  name: String,
  code: String,
  sub_category: Array,
});
const CampaignCategory = mongoose.model('CampaignCategory', CampaignCategorySchema);

export default CampaignCategory;