import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const TopProductSchema = new Schema({
  aff_link: String,
  price: String,
  discount: String,
  brand: String,
  category_id: String,
  category_name: String,
  desc: String,
  image: String,
  link: String,
  name: String,
  product_category: String,
  product_id: String,
  short_desc: String,
  updatedAt: Date,
  likes: [{ type: Schema.Types.ObjectId, ref: 'User' }]
}
);

const TopProduct = mongoose.model('TopProduct', TopProductSchema);

export default TopProduct;
