import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const CommentSchema = new Schema({
  belongTo: {
    kind: String,       // Product, Offer
    item: {
      type: ObjectId,
      refPath: 'belongTo.kind',
    }
  },
  author: {
    type: ObjectId,
    ref: 'User'
  },
  text: String,
  createAt: Date,
  status: String
}, {
  timestamps: true,
});

const CommentModel = mongoose.model('CommentModel', CommentSchema);
export default CommentModel;
