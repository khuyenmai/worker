import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
  at_product_link: String,
  billing: Number,
  browser: String,
  click_time: String,
  client_platform: String,
  confirmed_time: String,
  landing_page: String,
  merchant: String,
  order_id: String,
  order_pending: Number,
  order_reject: Number,
  order_success: Number,
  order_uid: String,
  product_category: String,
  products: [
    {
      _at: {
        banner_id: Number,
        commission_type: Number,
        goods_id: String,
        result_id: Number,
        reward_type: Number,
        seq_no: Number
      },
      _extra: {
        browser: String,
        device: String,
        device_brand: String,
        device_family: String,
        device_model: String,
        device_type: String,
        os: String,
      },
      amount: Number,
      campaign_id: String,
      click_time: Date,
      confirmed_time: Date,
      merchant: String,
      product_id: String,
      product_price: Number,
      product_quantity: Number,
      pub_commission: Number,
      sales_time: Date,
      status: Number
    }
  ],
  products_count: Number,
  pub_commission: Number,
  publisher: String,
  sales_time: Date,
  total_commission: Number,
  utm_campaign: String,
  utm_medium: String,
  utm_source: String,
  website: String,
  website_url: String,
  likes: [{ type: Schema.Types.ObjectId, ref: 'User' }]
});

const Order = mongoose.model('Order', OrderSchema);
export default Order;
