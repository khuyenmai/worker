'use strict';

import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const Schema = mongoose.Schema;

const serviceSchema = new Schema({
  provider: { type: String },
  user: {
    id: { type: String },
    last_name: { type: String },
    first_name: { type: String },
    name: { type: String },
    picture: {
      data: {
        url: { type: String },
        is_silhouette: { type: Boolean },
        width: { type: Number },
        height: { type: String }
      }
    },
    email: { type: String },
    accessToken: { type: String,}
  }
});

const UserSchema = new Schema({
  fullName: {
    type: String,
    trim: true,
    required: true,
  },
  email: {
    type: String,
    lowercase: true,
    trim: true,
    required: true,
  },
  hash_password: {
    type: String,
  },
  created: {
    type: Date,
    default: Date.now
  },
  playerId: {
    type: String,
  },
  following: [{ type: Schema.Types.ObjectId, ref: 'Campaign' }],
  service: { type: serviceSchema }
});

UserSchema.methods.comparePassword = function(password){
  return bcrypt.compareSync(password, this.hash_password);
};

const User = mongoose.model('User', UserSchema);

export default User;

